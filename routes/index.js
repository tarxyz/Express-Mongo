var express = require('express');
var Post = require('../models/post.js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    Post.get(null,function(err,posts){
        if(err){
            posts = [];
        }
        res.render('index',{
            title : '主页',
            //user : req.session.user,
            posts : posts,
            success : req.flash('success').toString(),
            error : req.flash('error').toString(),
        });
    });
});

module.exports = router;
