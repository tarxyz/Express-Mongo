var express = require('express');
var Post = require('../models/post.js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('postList', '');
});
/* GET publish page. */
router.get('/publish', function(req, res, next) {
    res.render('publish', '');
});
//发布文章功能
router.post('/publish',function(req,res,next){
    if(!req.session.user){
        req.flash('error','未登录');
        res.redirect('/users/login');
    }
    var currentUser = req.session.user;
    post = new Post(currentUser,req.body.title,req.body.post);
    post.save(function(err){
        if(err){
            req.flash('error',err);
            return res.redirect('/');
        }
        req.flash('success','发布成功');
        res.redirect('/');//发布成功跳转到主页
    });
});

module.exports = router;
